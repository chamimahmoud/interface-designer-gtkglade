#include <stdlib.h>
#include <gtk/gtk.h>
#include "callbacks.h"

 /*================================================
  * constantes
  *===============================================*/

#define NB_COLS 16
#define NB_ROWS 8

  /*================================================
   * Structure de donnes pour une Client
   *===============================================*/

typedef struct client {
    int x, y;  /* Position */
    int etat;  /* Etat */
    int dx;
} *Client;

typedef struct serveur {
    int x, y;  /* Position */
    int dx,dy; /* Deplacement */
} *Serveur;
/*================================================
 * Variables globales
 *===============================================*/

GtkImage* G_images[NB_ROWS][NB_COLS];
GdkPixbuf* G_pixbuf_tapis;
GdkPixbuf* G_pixbuf_Client;
GdkPixbuf* G_pixbuf_Client2;
GdkPixbuf* G_pixbuf_Serveur;



int   G_idle_timeout = 0; /* ID de l'evenement timeout */
Serveur serveur1;
Client G_Client2;
Client G_Client3;
Client G_Client4;
Client G_Client5;
/*================================================
 * Prototytpes des fonctions utilitaires
 *===============================================*/

void redessiner_grille();
void deplacer_Clients();
gboolean idle_function(gpointer user_data);
void start_animtation();
void stop_animtation();
void plus_animtation();
void moins_animtation();

/*================================================
 * Fonction principale
 *===============================================*/

int main(int argc, char* argv[]) {
    GtkBuilder* builder;
    GtkWindow* mainwindow;
    GtkTable* table;
    char txt[20];
    int i, j;

    gtk_init(&argc, &argv);

    /* Creer tous les widgets grace au builder GTK */
    builder = gtk_builder_new();
   gtk_builder_add_from_file(builder, "gtk_tablebis.glade", NULL);
   // gtk_builder_add_from_file(builder, "gtk_tab.glade", NULL);
    gtk_builder_connect_signals(builder, NULL);

    /*------------*/

    /*-- Recuperer les widgets construits par le builder GTK */
    mainwindow = GTK_WINDOW(gtk_builder_get_object(builder, "mainwindow"));
    table = GTK_TABLE(gtk_builder_get_object(builder, "table1"));

    /*-- Initialiser les widgets */

    /* Charger les images */
    G_pixbuf_Client = gdk_pixbuf_new_from_file("images/Client.png", NULL);
    G_pixbuf_Client2 = gdk_pixbuf_new_from_file("images/Client2.png", NULL);
    G_pixbuf_Serveur = gdk_pixbuf_new_from_file("images/Serveur.png", NULL);
    G_pixbuf_tapis = gdk_pixbuf_new_from_file("images/tapis.png", NULL);

    /* Redimensioner la grille d'agencement */
    gtk_table_resize(table, NB_COLS, NB_ROWS);

    /* Initialisation du tableau comme un "tapis vide" */
    for (i = 0; i < NB_ROWS; i++) {
        for (j = 0; j < NB_COLS; j++) {
            /* Creer un nouveau GtkImage */
            G_images[i][j] = (GtkImage*)gtk_image_new_from_pixbuf(G_pixbuf_tapis);

            /* Lui associer une etiquette avec ses coordonnees */
            sprintf(txt, "( %d ; %d )", j, i);
            gtk_widget_set_tooltip_text(GTK_WIDGET(G_images[i][j]), txt);

            /* Le placer dans la grille d'agencement prevue a cet effet */
            gtk_table_attach_defaults(table, (GtkWidget*)G_images[i][j], j, j + 1, i, i + 1);
        }
    }

    /* Creer la premiere Client */
    serveur1 = (struct serveur*)malloc(sizeof(struct serveur));

    /* Verifier le succes de l'allocation memoire */
    if (serveur1 == NULL) {
        perror("Erreur allocation memoire");
        exit(1);
    }

    /* Initialiser la Client */
    serveur1->x = NB_ROWS - 4;
    serveur1->y = 8;
    serveur1->dx = 0;
    serveur1->dy = 1;


    /* Creer la deuxieme Client */
    G_Client2 = (struct client*)malloc(sizeof(struct client));

    /* Verifier le succes de l'allocation memoire */
    if (G_Client2 == NULL) {
        perror("Erreur allocation memoire");
        exit(1);
    }

    /* Initialiser la Client */
    G_Client2->x = NB_ROWS - 2;
    G_Client2->y = NB_COLS - 2;
    G_Client2->dx = 1;
    G_Client2->etat = 0;



    /*Creer la troisieme Client*/
    G_Client3 = (struct client*)malloc(sizeof(struct client));

    /* Verifier le succes de l'allocation memoire */
    if (G_Client3 == NULL) {
        perror("Erreur allocation memoire");
        exit(1);
    }

    /* Initialiser la Client */
    G_Client3->x = NB_ROWS - 2;
    G_Client3->y = 4;
    G_Client3->dx = 1;
    G_Client3->etat = 0;
    
    /*Creer le quatrieme Client*/
    G_Client4 = (struct client*)malloc(sizeof(struct client));

    /* Verifier le succes de l'allocation memoire */
    if (G_Client4 == NULL) {
        perror("Erreur allocation memoire");
        exit(1);
    }

    /* Initialiser la Client */

    G_Client4->x = NB_ROWS - 6  ;
    G_Client4->y =  4;
    G_Client4->dx = 1;
    G_Client4->etat = 1;


    /*Creer le cinquieme Client*/
    G_Client5 = (struct client*)malloc(sizeof(struct client));

    /* Verifier le succes de l'allocation memoire */
    if (G_Client5 == NULL) {
        perror("Erreur allocation memoire");
        exit(1);
    }

    /* Initialiser la Client */

    G_Client5->x = NB_ROWS - 6;
    G_Client5->y = 14;
    G_Client5->dx = 1;
    G_Client5->etat = 0;
    /* Initialiser la matrice d'affichage */
    redessiner_grille();

    /*------------*/

    gtk_widget_show_all(GTK_WIDGET(mainwindow)); /* ATTENTION */

    /* Lancer la boucle GTK */
    gtk_main();

    /* La boucle GTK est terminee, detruire maintenant tous les widgets */
    g_object_unref(G_OBJECT(builder));

    /* Fin normale du programme */
    return 0;
}

/*================================================
 * Fonctions utilitaires
 *===============================================*/

void redessiner_grille() {
    int i, j;
    
  for (i = 0; i < NB_ROWS; i++) {
        for (j = 0; j < NB_COLS; j++) {

            /* Effacer l'image */ 
   gtk_image_clear(GTK_IMAGE(G_images[i][j]));

            if (i == serveur1->x && j == serveur1->y)
                gtk_image_set_from_pixbuf(GTK_IMAGE(G_images[i][j]), G_pixbuf_Serveur); /* Afficher Client  dans l'image (i,j) */
            else if (i == G_Client2->x && j == G_Client2->y && G_Client2->etat == 1)
                gtk_image_set_from_pixbuf(GTK_IMAGE(G_images[i][j]), G_pixbuf_Client2); /* Afficher Client  dans l'image (i,j) */
            else if (i == G_Client2->x && j == G_Client2->y && G_Client2->etat == 0)
                gtk_image_set_from_pixbuf(GTK_IMAGE(G_images[i][j]), G_pixbuf_Client);
            else if (i == G_Client3->x && j == G_Client3->y && G_Client3->etat == 1)
                gtk_image_set_from_pixbuf(GTK_IMAGE(G_images[i][j]), G_pixbuf_Client2); /* Afficher Client  dans l'image (i,j) */
            else if (i == G_Client3->x && j == G_Client3->y && G_Client3->etat == 0)
                gtk_image_set_from_pixbuf(GTK_IMAGE(G_images[i][j]), G_pixbuf_Client);
            else if (i == G_Client4->x && j == G_Client4->y && G_Client4->etat==1)
                gtk_image_set_from_pixbuf(GTK_IMAGE(G_images[i][j]), G_pixbuf_Client2); /* Afficher Client  dans l'image (i,j) */
            else if (i == G_Client4->x && j == G_Client4->y && G_Client4->etat == 0)
                gtk_image_set_from_pixbuf(GTK_IMAGE(G_images[i][j]), G_pixbuf_Client); /* Afficher Client  dans l'image (i,j) */
            else if (i == G_Client5->x && j == G_Client5->y && G_Client5->etat == 1)
                gtk_image_set_from_pixbuf(GTK_IMAGE(G_images[i][j]), G_pixbuf_Client2); /* Afficher Client  dans l'image (i,j) */
            else if (i == G_Client5->x && j == G_Client5->y && G_Client5->etat == 0)
                gtk_image_set_from_pixbuf(GTK_IMAGE(G_images[i][j]), G_pixbuf_Client);

           else
                gtk_image_set_from_pixbuf(GTK_IMAGE(G_images[i][j]), G_pixbuf_tapis); /* Afficher tapis dans l'image (i,j) */
        }
    }

    }
void gotoclient(Client G_Client) {
  
    if (serveur1->y != G_Client->y)
        serveur1->y += serveur1->dy;
  
    if (serveur1->x != G_Client->x - 1 && serveur1->y == G_Client->y) //permet que le robot decend
        serveur1->x += G_Client->dx;
}
void gotoclient1(Client G_Client) {
   
    if (serveur1->y != G_Client->y)
        serveur1->y -= serveur1->dy;
   
    if (serveur1->x != G_Client->x + 1 && serveur1->y == G_Client->y) //permet que le robot decend
        serveur1->x -= G_Client->dx;
}
void retval() {
   
    if (serveur1->y < 8) {
      
        serveur1->y++;
    }
    if (serveur1->y > 8) {
    
        serveur1->y--;
    }
    if (serveur1->y == 8) {
        if (serveur1->x > NB_ROWS - 4) {
       
            serveur1->x--;
        }
        if (serveur1->x < NB_ROWS - 4) {
       
            serveur1->x++;
        }
    }
}
int i = 0;
void deplacer_Clients() {

    /* Verifier que le deplacement de la Client 1 en x sera valide (sinon prendre direction opposee) */
  //  if (serveur1->x + serveur1->dx < 0 || serveur1->x + serveur1->dx >= NB_ROWS)


    /* Verifier que le deplacement de la Client 1 en y sera valide (sinon prendre direction opposee) */
    if (serveur1->y + serveur1->dy < 0 || serveur1->y + serveur1->dy >= NB_COLS)
        serveur1->dy = -serveur1->dy;
    if (G_Client2->etat == 1) {
        gotoclient(G_Client2);
     if (serveur1->x == G_Client2->x - 1 && serveur1->y == G_Client2->y)
            G_Client2->etat = 0;
    }
    if (G_Client2->etat == 0 && G_Client3->etat == 0 && G_Client4->etat == 0 && G_Client5->etat == 0)
        retval();
    if (G_Client3->etat == 1) {
        gotoclient(G_Client3);
        if (serveur1->x == G_Client3->x - 1 && serveur1->y == G_Client3->y)
            G_Client3->etat = 0;
            
    }
    if (G_Client4->etat == 1) {
        gotoclient1(G_Client4);
        if (serveur1->x == G_Client4->x + 1 && serveur1->y == G_Client4->y)
            G_Client4->etat = 0;
    }
    if (G_Client5->etat == 1) {
        gotoclient1(G_Client5);
        if (serveur1->x == G_Client5->x + 1 && serveur1->y == G_Client5->y)
            G_Client5->etat = 0;
    }
    i++;
    if (i == 20) {
        G_Client3->etat = 1;
    }
    if (i == 50) {
        G_Client5->etat = 1;
    }
    if (i == 80) {
        G_Client2->etat = 1;
    }
    if (i == 130) {
        G_Client4->etat = 1;
    }

 
}

gboolean idle_function(gpointer user_data) {
        deplacer_Clients();
        redessiner_grille();
    return TRUE; /* should return FALSE if the source should be removed */
}

void start_animtation() {
    if (G_idle_timeout == 0) {
        /* Demander que la fonction idle_function() soit appellee toutes les 200 ms */
            G_idle_timeout = g_timeout_add(200, (GSourceFunc)idle_function, NULL);
       
    }
}
void plus_animtation() {
    if (G_idle_timeout == 0) {
        /* Demander que la fonction idle_function() soit appellee toutes les 200 ms */
        G_idle_timeout = g_timeout_add(50, (GSourceFunc)idle_function, NULL);

    }
}
void moins_animtation() {
    if (G_idle_timeout == 0) {
        /* Demander que la fonction idle_function() soit appellee toutes les 200 ms */
        G_idle_timeout = g_timeout_add(1000, (GSourceFunc)idle_function, NULL);

    }
}
void stop_animtation() {
    if (G_idle_timeout > 0) {
        /* Demander de ne plus appeller la fonction idle_function() toutes les 200 ms */
        g_source_remove(G_idle_timeout);
        G_idle_timeout = 0;
    }
}





