#ifndef __CALLBACKS_H
#define __CALLBACKS_H

G_MODULE_EXPORT void on_mainwindow_destroy(GtkObject* object, gpointer user_data);
G_MODULE_EXPORT void on_button_start_clicked(GtkObject* object, gpointer user_data);
G_MODULE_EXPORT void on_button_stop_clicked(GtkObject* object, gpointer user_data);
G_MODULE_EXPORT void on_button_plus_clicked(GtkObject* object, gpointer user_data);
G_MODULE_EXPORT void on_button_moins_clicked(GtkObject* object, gpointer user_data);

#endif /* __CALLBACKS_H */
