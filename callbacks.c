#include <gtk/gtk.h>
#include "callbacks.h"

void stop_animtation();
void start_animtation();
void moins_animtation();
void plus_animtation();

/*================================================
 * Fonctions callbacks
 *===============================================*/

G_MODULE_EXPORT void on_mainwindow_destroy(GtkObject* object, gpointer user_data) {
	/* Arreter la boucle d'animation */
	stop_animtation();

	/* Stopper la boucle GTK */
	gtk_main_quit();
}

G_MODULE_EXPORT void on_button_start_clicked(GtkObject* object, gpointer user_data) {
	start_animtation();
}

G_MODULE_EXPORT void on_button_stop_clicked(GtkObject* object, gpointer user_data) {
	stop_animtation();
}
G_MODULE_EXPORT void on_button_plus_clicked(GtkObject* object, gpointer user_data) {
	plus_animtation();
}
G_MODULE_EXPORT void on_button_moins_clicked(GtkObject* object, gpointer user_data) {
	moins_animtation();
}

